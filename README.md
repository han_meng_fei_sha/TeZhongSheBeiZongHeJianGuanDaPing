# 特种设备综合监管大屏

#### 介绍：
数据可视化大屏-特种设备综合监管 压力容器  
https://blog.csdn.net/yue31313/article/details/90813821


1、集成高德地图 地理信息系统（GIS）。显示图标点、点击显示弹窗。更改地图主题背景色。街道。复选框，筛选切换显示类型状态。

2、echarts图表。地图。占比统计。

3、数据转换成图表所用数据，代码逻辑优化。提高数据生成图表开发效率。



#### 源码下载：
https://gitee.com/han_meng_fei_sha/TeZhongSheBeiZongHeJianGuanDaPing/tree/master

（多分享，让世界进步得更快，生活更加美好。希望可以帮助到大家。希望大家也多分享。）



#### 界面：
特种设备综合监管大屏20190604214315
![](https://images.gitee.com/uploads/images/2019/0604/221846_bf246d9b_1462159.png)



特种设备综合监管大屏-点击弹窗20190604214314

![](https://images.gitee.com/uploads/images/2019/0604/221851_0e32a809_1462159.png)

压力容器监管大屏20190604214456
![](https://img-blog.csdnimg.cn/20190604214645575.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3l1ZTMxMzEz,size_16,color_FFFFFF,t_70)


压力容器监管大屏-放大20190604214457

![](https://images.gitee.com/uploads/images/2019/0604/221846_f92a6d2b_1462159.png)


excel表格数据转换成json形式数据-在线工具：

https://echarts.baidu.com/spreadsheet.html

 

后期可优先完善的地方：

1、界面动态效果。边框动画、地图动效。图表动效。

2、地图上海量点，千万级别的数据的展示。热力分布效果。


作者：韩亚飞_yue31313_韩梦飞沙   
QQ:313134555  
原文：https://blog.csdn.net/yue31313/article/details/90813821 